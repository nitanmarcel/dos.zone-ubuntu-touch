/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERSETTINGS_H
#define PLAYERSETTINGS_H

#include <QObject>

class PlayerSettings: public QObject {
    Q_OBJECT

public:
    PlayerSettings();
    ~PlayerSettings() = default;

    // force_hardware_acceleration
    Q_INVOKABLE void setForceHardwareAcceleration(bool state);
    Q_INVOKABLE void setIgnoreGpuBlacklist(bool state);

    Q_INVOKABLE bool getForceHardwareAcceleration();
    Q_INVOKABLE bool getIgnoreGpuBlacklist();
};

#endif
