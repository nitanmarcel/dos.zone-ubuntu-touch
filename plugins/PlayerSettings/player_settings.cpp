/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QSettings>

#include "player_settings.h"

QSettings settings(QSettings::UserScope, "doszone.nitanmarcel", "doszone");

PlayerSettings::PlayerSettings() {

}

void PlayerSettings::setForceHardwareAcceleration(bool state) {
    settings.setValue("force_hardware_acceleration", state);
}

void PlayerSettings::setIgnoreGpuBlacklist(bool state) {
    settings.setValue("ignore_gpu_blacklist", state);
}

bool PlayerSettings::getForceHardwareAcceleration() {
    return settings.value("force_hardware_acceleration", false).value<bool>();
}

bool PlayerSettings::getIgnoreGpuBlacklist() {
    return settings.value("ignore_gpu_blacklist", false).value<bool>();
}