/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QQuickStyle>
#include <QDebug> 
#include <QString>
#include <QSettings>

QSettings settings(QSettings::UserScope, "doszone.nitanmarcel", "doszone");
int main(int argc, char *argv[])
{   
    QString flags = qgetenv("QTWEBENGINE_CHROMIUM_FLAGS");
    if (settings.value("force_hardware_acceleration", false).value<bool>())
    {
        flags.append(" --enable-gpu-rasterization --enable-native-gpu-memory-buffers --enable-accelerated-video-decode");
    }
    if (settings.value("ignore_gpu_blacklist", false).value<bool>()) {
        flags.append(" --ignore-gpu-blacklist ");
    }
    
    qputenv("QTWEBENGINE_CHROMIUM_FLAGS", flags.toLocal8Bit());


    QGuiApplication::setApplicationName("Dos.Zone");
    QGuiApplication::setOrganizationName("doszone.nitanmarcel");
    QGuiApplication::setApplicationName("doszone.nitanmarcel");
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    QQuickStyle::setStyle("Suru");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("availableStyles", QQuickStyle::availableStyles());
    engine.load(QUrl("qml/Main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app->exec();
}
