# Dos.Zone

Unofficial webapp for dos.zone. Ultimate collection of free dos games to play online in browser.

App logo licensed under Expat/MIT License. Source https://commons.wikimedia.org/wiki/File:Msdos-icon.svg

## License

Copyright (C) 2022  Marcel Alexandru Nitan

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
