/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.2

Page {
    anchors.fill: parent
    Column {
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            text: "Experimental settings";
            font.bold: true
            wrapMode: Text.WordWrap
            width: parent.width
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        Switch {
            text: "Force hardware acceleration"
            checked: forceHardwareAcceleration
            onClicked: () => {forceHardwareAcceleration = !forceHardwareAcceleration; restartDialog.open()}
        }
        Switch {
            text: "Ignore GPU Blacklist"
            checked: ignoreGpuBlacklist
            onClicked: () => {ignoreGpuBlacklist = !ignoreGpuBlacklist; restartDialog.open()}
        }

        Button {
            text: "Debug"
            onClicked: () => stackView.push("DebugPage.qml")
        }
    }
}
