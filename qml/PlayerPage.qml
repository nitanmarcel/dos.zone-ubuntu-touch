/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.3
import QtWebEngine 1.8

Page {
    id: dos
    anchors.fill: parent
    property string bundleUrl;

    WebEngineView {
        id: webView
        anchors.fill: parent
        url: bundleUrl

        settings.accelerated2dCanvasEnabled: true

        profile: WebEngineProfile {
            httpUserAgent: "Mozilla/5.0 (Linux; Android 11; SM-A125U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Mobile Safari/537.36"
            storageName: "Storage"
            persistentStoragePath: "/home/phablet/.local/share/doszone.nitanmarcel/QWebEngine"
        }

        onFullScreenRequested: (request) => {
            window.isFullscreenState = !window.isFullscreenState
            request.accept()
        }
    }

    Rectangle {
        id: webViewLoadingIndicator
        anchors.fill: parent
        z: 1
        color: Suru.backgroundColor
        visible: webView.loading === true

        BusyIndicator {
            id: busy
            anchors.centerIn: parent
        }
    }
}