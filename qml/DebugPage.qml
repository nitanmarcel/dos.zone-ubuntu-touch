import QtQuick 2.7
import QtQuick.Controls 2.2
import QtWebEngine 1.8

Page {
    anchors.fill: parent
    WebEngineView {
        anchors.fill: parent
        url: "chrome://gpu"
        onNewViewRequested: function (request) {
             Qt.openUrlExternally(request.requestedUrl)
        }
        onNavigationRequested: function (request) {
            if (request.url.toString() !== "chrome://gpu/")
            {
                Qt.openUrlExternally(request.requestedUrl)
                request.action = WebEngineNavigationRequest.IgnoreRequest
            }       
        }
    }
}