/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * doszone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0

ListModel {
    ListElement {
        gameName : "Arcade Volleyball"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/b/bc2f7ea2cf2738e236ce5043b6c756440e157949.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Arkanoid"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/3/3c2bda09093577bd865efb0b5b5b4fdc69051c54.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Beneath a Steel Sky"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/9/9392bef006fcb485bd851fe3859bbeec659bbcf0.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Blood"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/blood-lowres.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Bomberman"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/3/320c1d875fba0f53476ae188195d2bec2cefdf8b.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "CD-Man Version 2.0"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/3/3d082ee2ea19b168cb93244fc0e771e12af585f3.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Chex Quest"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/a/a580eccc17fca3235d67cf85331e7cb204eb4899.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Choo Choo Minder"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/choo-choo-minder.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Color Lines"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/f/fce2566b9fd55f562dab5127a02804bc15697f53.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Cyborgirl"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/2/20c58161b8f82399dfdbf5f5d700cea1e5deaf34.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "DOOM"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/doom.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "DOOM II"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/doom2.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Dangerous Dave in the Haunted Mansion"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/6/6a2bfa87c031c2a11ab212758a5d914f7c112eeb.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Deluxe Ski Jump"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/d/d5d62e6309d4f05d5e1967416b875b4fed8367ce.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Digger"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/2/24b00b14f118580763440ecaddcc948f8cb94f14.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Disney's Aladdin"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/6/64ae157f1baa4317f626ccbc74364d9da87d5558.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Duke Nukem 3D"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/duke3d_320.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Duke Nukem II"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/d/d0e7675507a09a26c017996d0994da4ee38c90fe.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Earthworm Jim"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/a/aad1d125300d7d93bc28058fa4d0247a7142510e.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Final DOOM"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/4/49ea7e89c84d01ae630772b4d90e37927955522a.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Golden Axe"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/a/ad3686df58a0bac357d3bb81b3f2536205e9ad76.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Grand Theft Auto"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/gta-mobile.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Heroes Of Might And Magic II"
        gameUrl : "http://dos.zone/player/?bundleUrl=https%3A%2F%2Fdoszone-uploads.s3.dualstack.eu-central-1.amazonaws.com%2Fcustom%2Fdos%2Fhomm_2.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Indianapolis 500: The Simulation"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/4/48ab323233babd8c55352031214c06265da2aea0.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Mortal Kombat"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/8/872f3668c36085d0b1ace46872145285364ee628.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Oregon Trail Deluxe"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/5/53e616496b4da1d95136e235ad90c9cc3f3f760d.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Out of This World"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/1/1031eb810e8b648fc5f777b3bd9cbc0187927fd4.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Pac Man"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/5/5cdcffbf268b3be0555025902b52a8d21ad595b9.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Pole Chudes: Capital Show"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/5/5f21f221e023bbca253396824c5f652438eeef8c.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Prehistorik"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/7/73ff69232f4002228e74f73abb7e62299a2a8f3f.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Prehistorik 2"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/f/f460331d06f443fc58ad21fc4824a74716270243.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Prince of Persia"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/1/1179a7c9e05b1679333ed6db08e7884f6e86c155.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Prince of Persia 2: The Shadow & The Flame"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/9/9ce632235395211854a728cf49372bc12b66f628.jsdos&anonymous=1&networking=1"
    }
    // ListElement {
    //     gameName : "SeXoniX"
    //     gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/6/6cbb853f5efb658f0381104c398f90005459ed86.jsdos&anonymous=1&networking=1"
    // }
    // ListElement {
    //     gameName : "Sexy Droids"
    //     gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/sexy-droids.jsdos&anonymous=1&networking=1"
    // }
    ListElement {
        gameName : "Sim City"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/7/744842062905f72648a4d492ccc2526d039b3702.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Sim City 2000"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/b/b1ed3b93829bdff0c9062c5642767825dd52baf1.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Space Quest: Chapter I - The Sarien Encounter"
        gameUrl : "http://dos.zone/player/?bundleUrl=https%3A%2F%2Fdoszone-uploads.s3.dualstack.eu-central-1.amazonaws.com%2Fcustom%2Fdos%2Fspace-quest-chapter-i-the-sarien-encounter.jsdos&anonymous=1&networking=1"
    }
    // ListElement {
    //     gameName : "Strip Poker Professional: Rev B"
    //     gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/strip-poker-professional-rev-b.jsdos&anonymous=1&networking=1"
    // }
    ListElement {
        gameName : "Supaplex"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/5/5f080ad8d3d06df0d4a5583a221e36dc79eeddd9.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Test Drive"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/a/abcf902327ce1cf6fe7b3e8c809d638984377d7c.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Tetris"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/3/37ce97891bc876adc2cbb7e44e4018c95644a19c.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "The Incredible Machine"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/incredible-machine.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "The Incredible Machine 2"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/f/fd74b5f9cdc413e64cbc6d4f20d86a8f726f4e17.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "The Lost Vikings"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/1/1b063b2520052ebb504184667ac95e72423331de.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "The Need for Speed"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/nfs.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "The Ultimate DOOM"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/ultimate-doom.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Toppler"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/4/4426757ae08c0e7f21193d04a2ad0982e83c3dbf.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Transport Tycoon Deluxe"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/6/60b165c86771eadf24cb2f81aef4656b85d167a6.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Volfied"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/9/9325b7ea382dbfe7ffbc84dc74fcd98888e6e6ab.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Wolfendoom"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/wolfendoom.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "Wolfenstein 3D"
        gameUrl : "http://dos.zone/player/?bundleUrl=https%3A%2F%2Fdoszone-uploads.s3.dualstack.eu-central-1.amazonaws.com%2Foriginal%2F2X%2Fa%2Fac888d1660aa253f0ed53bd6c962c894125aaa19.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "X-COM: Apocalypse"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/xcom3-eng.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "X-COM: Terror from the Deep"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/custom/dos/xcom2-eng.jsdos&anonymous=1&networking=1"
    }
    ListElement {
        gameName : "X-COM: UFO Defense"
        gameUrl : "http://dos.zone/player/?bundleUrl=https://cdn.dos.zone/original/2X/3/373503784811ac8505dc2fcc3e241fc60493171a.jsdos&anonymous=1&networking=1"
    }
}